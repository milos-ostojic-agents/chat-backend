package beans;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import models.Host;
import models.Message;
import models.NewHostInfo;
import models.User;

@Stateless
public class RestClientBean {
	
	public NewHostInfo registerNode(Host newHost, Host master) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(master.getAddress() + "/rest/cluster/register");
		
		Response response = target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(newHost, MediaType.APPLICATION_JSON));
		
		return response.readEntity(NewHostInfo.class);
	}
	
	public void nodeRegistered(Host newHost, Host targetHost) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(targetHost.getAddress() + "/rest/cluster/node");
		
		target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(newHost, MediaType.APPLICATION_JSON));
		
	}
	
	public void sendLoggedInUsers(List<User> users, Host targetHost) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(targetHost.getAddress() + "/rest/cluster/users/loggedIn");
		
		target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(users, MediaType.APPLICATION_JSON));
	}
	
	public void deleteNode(String alias, Host targetHost) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(targetHost.getAddress() + "/rest/cluster/node/" + alias);
		
		target.request(MediaType.APPLICATION_JSON)
				.delete();
	}
	
	public boolean isHostAlive(Host targetHost) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(targetHost.getAddress() + "/rest/cluster/node/");
		
		Response response = target.request(MediaType.TEXT_PLAIN).get();
		String result = response.readEntity(String.class);
		return result.equals("Ok");
	}
	
	public void sendMessage(Message message, Host targetHost) {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(targetHost.getAddress() + "/rest/chat/messages/user");
		
		target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(message, MediaType.APPLICATION_JSON));
	}
}
