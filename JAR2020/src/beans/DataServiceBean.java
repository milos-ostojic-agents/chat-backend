package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import models.Host;
import models.Message;
import models.User;

@Singleton
@LocalBean
public class DataServiceBean {
	private String myAlias;
	private List<User> registeredUsers = new ArrayList<User>();
	private List<User> loggedInUsers = new ArrayList<User>();
	private List<Message> messages = new ArrayList<Message>();
	private List<Host> hosts = new ArrayList<Host>();
	
	public void setMyAlias(String alias) {
		myAlias = alias;
	}
	
	public String getMyAlias() {
		return myAlias;
	}
	
	// client methods
	public boolean registerUser(String username, String password) {
		boolean usernameExists = registeredUsers.stream().anyMatch(u -> u.getUsername().equals(username));
		if (usernameExists) return false;
		
		User newUser = new User(username, password, myAlias);
		this.registeredUsers.add(newUser);
		return true;
	}
	
	public boolean logInUser(String username, String password) {
		if (loggedInUsers.stream().anyMatch(u -> u.getUsername().equals(username))) return true;
		
		Optional<User> user = registeredUsers.stream()
								   .filter(u -> u.getUsername().equals(username) &&
										        u.getPassword().equals(password)).findFirst();
		
		if (!user.isPresent()) return false;
		
		loggedInUsers.add(user.get());
		return true;
	}
	
	public List<User> getLoggedInUsers() {
		return this.loggedInUsers;
	}
	
	public List<User> getRegisteredUsers() {
		return this.registeredUsers;
	}
	
	public Host getUsersHost(String username) {
		for (User user : loggedInUsers) {
			if (!user.getUsername().equals(username)) continue;
			
			if (user.getHostAlias().equals(myAlias)) return null;
			
			for (Host host : hosts) {
				if (host.getAlias().equals(user.getHostAlias())) return host;
			}
		}
		
		return null;
	}
	
	public List<Message> sendMessageToAllUsers(Message message, boolean onlyLoggedIn) {
		List<User> targetedUsers = onlyLoggedIn ? loggedInUsers
												: registeredUsers;
		
		List<Message> sentMessages = new ArrayList<Message>();
		for (User user : targetedUsers) {
			if (user.getUsername().equals(message.getSender())) continue;
			
			Message newMessage = new Message(user.getUsername(), 
								  		  	 message.getSender(), 
								  		  	 new Date(),
								  		  	 message.getSubject(),
								  		  	 message.getContent());
			messages.add(newMessage);
			sentMessages.add(newMessage);
		}
		
		return sentMessages;
	}
	
	public void sendMessage(Message message) {
		this.messages.add(message);
	}
	
	public List<Message> getAllMessagesForUser(String username) {
		Stream<Message> messages = this.messages.stream().filter(m -> m.getRecipient().equals(username) ||
																	    m.getSender().equals(username));
		
		return messages.collect(Collectors.toList());
	}
	
	public void logOutUser(String username) {
		for (User user : loggedInUsers) {
			if (user.getUsername().equals(username)) {
				loggedInUsers.remove(user);
				return;
			}
		}
	}
	
	public void setLoggedInUsers(List<User> users) {
		this.loggedInUsers = users;
	}
	
	//cluster methods
	public void registerNode(Host host) {
		hosts.add(host);
	}
	
	public void registerNodes(List<Host> hosts) {
		this.hosts.addAll(hosts);
	}
	
	public Host getNextNode(String alias) {
		int size = hosts.size();
		if (size == 1) return null;
		
		for (int i = 0; i < size; i++) {
			if (!hosts.get(i).getAlias().equals(alias)) continue;
			
			return i == size - 1 ? hosts.get(0) : hosts.get(i + 1);
		}
		
		return null;
	}
	
	public List<Host> getRegisteredNodes() {
		return hosts.stream()
					.filter(h -> !h.getAlias().equals(myAlias))
					.collect(Collectors.toList());
	}
	
	public void deleteNode(String alias) {
		for (Host host : hosts) {
			if (host.getAlias().equals(alias)) {
				hosts.remove(host);
				return;
			}
		}
		
		loggedInUsers = loggedInUsers.stream()
								     .filter(u -> !u.getHostAlias().equals(alias))
								     .collect(Collectors.toList());
	}
}
