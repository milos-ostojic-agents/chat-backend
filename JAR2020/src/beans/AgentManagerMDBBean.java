package beans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import models.ACLMessage;
import models.AID;
import models.Agent;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/agentInternalQueue")
})
public class AgentManagerMDBBean implements MessageListener {
	@EJB AgentDataService dataService;
	
	@Override
	public void onMessage(Message msg) {
		ObjectMessage objMessage = (ObjectMessage)msg;
		try {
			ACLMessage acl = (ACLMessage)objMessage.getObject();

			for (AID aid: acl.getReceivers()) {
				Agent agent = dataService.getAgent(aid.getName());
				if (agent == null) continue;
				agent.handleMessage(acl);
			}			
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
