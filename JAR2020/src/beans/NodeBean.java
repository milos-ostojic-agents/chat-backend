package beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import models.Host;
import models.NewHostInfo;
import models.User;
// import ws.WSEndPoint;

@Stateless
@Path("/cluster")
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
public class NodeBean {
	@EJB
	DataServiceBean dataService;
	@EJB
	RestClientBean client;
	// @EJB
	// WSEndPoint wsEndPoint;
	@EJB
	JMSBean jms;
	
	@POST
	@Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	public NewHostInfo registerNode(Host host) {
		List<Host> nodes = dataService.getRegisteredNodes();
		dataService.registerNode(host);
		
		for (Host node : nodes) {
			client.nodeRegistered(host, node);
		}
		
		List<User> loggedInUsers = dataService.getLoggedInUsers();
		return new NewHostInfo(loggedInUsers, nodes);
	}
	
	@POST
	@Path("/node")
	public void nodeRegistered(Host host) {
		dataService.registerNode(host);
	}
	
	@POST
	@Path("/users/loggedIn")
	public void loggedInUsers(List<User> users) {
		dataService.setLoggedInUsers(users);
		
		// wsEndPoint.sendUserList(users);
		jms.sendJMSMessage(new ArrayList<User>(users));
	}
	
	@DELETE
	@Path("/node/{alias}")
	public void deleteNode(@PathParam("alias") String alias) {
		dataService.deleteNode(alias);
	}
	
	@GET
	@Path("/node")
	@Produces(MediaType.TEXT_PLAIN)
	public String heartbeat() {
		return "Ok";
	}
}
