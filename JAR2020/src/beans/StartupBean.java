package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import models.Host;
import models.NewHostInfo;

@Startup
@Singleton
public class StartupBean {
	@EJB
	DataServiceBean dataService;
	@EJB
	RestClientBean client;
	
	@PostConstruct
	public void init() {
		Host myself = new Host("master", "http://192.168.1.158:8080/WAR2020");
		Host master = new Host("master", "http://192.168.1.158:8080/WAR2020");
		
		dataService.setMyAlias(myself.getAlias());
		dataService.registerNode(myself);
		
		if (myself.getAlias().equals("master")) return;
		
		dataService.registerNode(master);
		NewHostInfo info = client.registerNode(myself, master);
		dataService.setLoggedInUsers(info.getLoggedInUsers());
		dataService.registerNodes(info.getRegisteredNodes());
	}
}
