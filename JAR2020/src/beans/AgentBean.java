package beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import models.ACLMessage;
import models.Agent;
import models.AgentType;
import models.Performative;

@Stateless
@LocalBean
@Path("/main")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AgentBean {
	
	@EJB
	AgentDataService dataService;
	
	@EJB
	JMSAgentBean jms;
	
	@GET
	@Path("/agents/classes")
	public List<AgentType> getAgentTypes() {
		return dataService.getAgentTypes();
	}
	
	@GET
	@Path("/agents/running")
	public List<Agent> getRunningAgents() {
		return dataService.getRunningAgents();
	}
	
	@PUT
	@Path("/agents/running/{type}/{name}")
	public Agent startAgent(@PathParam("type") String typeName, @PathParam("name") String agentName) {	
		Agent newAgent = dataService.addAgent(typeName, agentName);
		
		jms.sendJMSMessage(new ArrayList<Agent>(dataService.getRunningAgents()), false);
		
		return newAgent;
	}
	
	@DELETE
	@Path("/agents/running/{aid}")
	public void stopAgent(@PathParam("aid") String aidName) {
		dataService.killAgent(aidName);
		
		jms.sendJMSMessage(new ArrayList<Agent>(dataService.getRunningAgents()), false);
	}
	
	@POST
	@Path("/messages")
	public void postMessage(ACLMessage message) {
		Agent agent = dataService.getAgent(message.getSender().getName());
		agent.handleMessage(message);
	}
	
	@GET
	@Path("/messages")
	public ArrayList<String> getPerformatives() {
		ArrayList<String> performatives = new ArrayList<String>();
		for(Performative performative : Performative.values()) {
			performatives.add(performative.toString());
		}
		
		return performatives;
	}
}
