package beans;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import models.AID;
import models.Agent;
import models.AgentType;

@Singleton
@LocalBean
public class AgentDataService {
	private List<Agent> agents = new ArrayList<Agent>();
	
	public List<AgentType> getAgentTypes() {
		ArrayList<AgentType> types = new ArrayList<AgentType>();
		
		try {
			InitialContext context = new InitialContext();
			NamingEnumeration<NameClassPair> list = context.list("java:module");
			while (list.hasMore()) {
				String fullName = list.next().getName();
				
				if (fullName.contains("!agents")) {
					types.add(new AgentType(fullName.split("!")[0], "agents"));
				}
			}			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return types;
	}
	
	public List<Agent> getRunningAgents() {
		return agents;
	}
	
	public Agent getAgent(String aidName) {
		for (Agent agent: agents) {
			if (agent.getId().getName().equals(aidName)) return agent;
		}
		
		return null;
	}
	
	public Agent addAgent(String typeName, String agentName) {
		try {
			InitialContext context = new InitialContext();
			Agent agent = (Agent) context.lookup("java:module/" + typeName);
			AgentType type = null;
			for (AgentType t: getAgentTypes()) {
				if (t.getName().equals(typeName)) {
					type = t;
					break;
				}
			}
			if (type == null) return null;

			agent.setId(new AID(agentName, "master", type));
			addAgent(agent);
			
			return agent;
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void addAgent(Agent agent) {
		agents.add(agent);
	}
	
	public void killAgent(String aidName) {
		for (Agent agent : agents) {
			if (!agent.getId().getName().equals(aidName)) continue;
			
			agents.remove(agent);
			return;
		}
	}
}
