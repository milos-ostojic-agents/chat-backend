package beans;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;

@Stateless
public class JMSAgentBean {
	@Resource(mappedName = "java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	@Resource(mappedName = "java:jboss/exported/jms/queue/agentQueue")
	private Queue queue;
	
	@Resource(mappedName = "java:jboss/exported/jms/queue/agentInternalQueue")
	private Queue internalQueue;
	
	public void sendJMSMessage(Serializable payload, boolean internal) {
		try {
			Queue selectedQueue = internal ? internalQueue : queue;
			QueueConnection connection = (QueueConnection) connectionFactory.createConnection("guest", "guest.guest.1");
			QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			QueueSender sender = session.createSender(selectedQueue);
			
			ObjectMessage message = session.createObjectMessage();
			message.setObject(payload);
			sender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
