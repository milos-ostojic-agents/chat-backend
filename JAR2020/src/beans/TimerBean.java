package beans;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import models.Host;

@Singleton
public class TimerBean {
	@EJB
	RestClientBean client;
	@EJB
	DataServiceBean dataService;
	
	@Schedule(hour = "*/1")
	public void heartbeatCheck() {
		// every node checks if the next one in the list is alive
		Host host = dataService.getNextNode(dataService.getMyAlias());
		if (host == null) return; 
		
		if (client.isHostAlive(host)) return;
		if (client.isHostAlive(host)) return;
		
		List<Host> hosts = dataService.getRegisteredNodes();		
		for (Host h : hosts) {
			if (h.getAlias().equals(host.getAlias())) continue;
			
			client.deleteNode(host.getAlias(), h);
		}
		
		dataService.deleteNode(host.getAlias());
	}
	
}
