package helpers;

import java.util.List;

import models.ACLMessage;
import models.Agent;
import models.Message;
import models.User;

public class Parser {
	public String messageToJSON(Message message) {
		return "{ \"recipient\": \""+ message.getRecipient() +
				"\", \"sender\": \""+ message.getSender() +
				"\", \"creationDate\": \""+ message.creationDateToString() +
				"\", \"subject\": \""+ message.getSubject() +
				"\", \"content\": \""+ message.getContent() +"\" }";
	}
	
	public String userToJSON(User user) {
		return "{ \"username\": \""+ user.getUsername() +
				"\", \"password\": \""+ user.getPassword() +
				"\", \"hostAlias\": \""+ user.getHostAlias() +"\" }";
	}
	
	public String userListToJSON(List<User> users) {
		String result = "[";
		for (User user : users) {
			result += userToJSON(user);
			if (users.indexOf(user) < users.size() - 1)	result += ",";
		}
		result += "]";
		
		return result;
	}
	
	public String agentListToJSON(List<Agent> agents) {
		String result = "[";
		for (Agent agent: agents) {
			result += agentToJSON(agent);
			if (agents.indexOf(agent) < agents.size() - 1) result += ",";
		}
		result += "]";
		
		return result;
	}
	
	public String agentToJSON(Agent agent) {
		return "{ \"name\": \""+ agent.getId().getName() +
				"\", \"type\": \""+ agent.getId().getType().getName() + "\" }";
	}
	
	public String aclToJSON(ACLMessage message) {
		return "{ \"performative\": \""+ message.getPerformative().toString() +
				"\", \"sender\": \""+ message.getSender().getName() +
				"\", \"receivers\": \""+ message.getReceivers()[0].getName() + "\" }";
	}
}
