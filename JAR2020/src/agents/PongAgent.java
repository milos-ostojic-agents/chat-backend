package agents;

import javax.ejb.Stateful;

import models.ACLMessage;
import models.AID;
import models.Agent;
import models.Performative;

@Stateful
public class PongAgent extends Agent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void handleMessage(ACLMessage message) {
		if (!message.getPerformative().equals(Performative.REQUEST)) return;
		
		ACLMessage response = new ACLMessage();
		response.setPerformative(Performative.INFORM);
		response.setSender(getId());
		response.setReceivers(new AID[] { message.getSender() });
		
		jms.sendJMSMessage(message, false);
		jms.sendJMSMessage(response, true);		
	}
}
