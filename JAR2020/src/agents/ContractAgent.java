package agents;

import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Stateful;

import beans.JMSAgentBean;
import models.ACLMessage;
import models.AID;
import models.Agent;
import models.Performative;

@Stateful
public class ContractAgent extends Agent {

	@EJB
	JMSAgentBean jms;
	
	public void handleMessage(ACLMessage message) {
		ACLMessage response = new ACLMessage();
		response.setSender(getId());
		response.setReceivers(new AID[] { message.getSender() });		
		
		if (message.getPerformative().equals(Performative.CALL_FOR_PROPOSAL)) {
			response.setPerformative(Performative.PROPOSE);
		} else if (message.getPerformative().equals(Performative.PROPOSE)) {
			Random rand = new Random();
		    int int_random = rand.nextInt(100);
			response.setPerformative(int_random < 50 ? Performative.ACCEPT_PROPOSAL
												     : Performative.REJECT_PROPOSAL);
		} else {
			jms.sendJMSMessage(message, false);
			return;
		}
		
		jms.sendJMSMessage(message, false);
		jms.sendJMSMessage(response, true);
	}
}
