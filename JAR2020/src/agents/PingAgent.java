package agents;

import javax.ejb.EJB;
import javax.ejb.Stateful;

import beans.JMSAgentBean;
import models.ACLMessage;
import models.Agent;
import models.Performative;
import models.AID;

@Stateful
public class PingAgent extends Agent {
	
	@EJB
	JMSAgentBean jms;
	
	public void handleMessage(ACLMessage message) {
		if (message.getPerformative().equals(Performative.REQUEST)) {			
			jms.sendJMSMessage(message, true);
		} else {
			jms.sendJMSMessage(message, false);
		}	
		
	}
}
