package models;

import java.io.Serializable;

public class AID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String hostAlias;
	private AgentType type;
	
	public AID() {}
	
	public AID(String name, String hostAlias, AgentType type) {
		this.name = name;
		this.hostAlias = hostAlias;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostAlias() {
		return hostAlias;
	}

	public void setHostAlias(String hostAlias) {
		this.hostAlias = hostAlias;
	}

	public AgentType getType() {
		return type;
	}

	public void setType(AgentType type) {
		this.type = type;
	}
	
	
}

