package models;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String recipient;
	private String sender;
	private Date creationDate;
	private String subject;
	private String content;
	
	public Message() {}
	
	public Message(String receiver, String sender, Date creationDate, String subject, String content) {
		super();
		this.recipient = receiver;
		this.sender = sender;
		this.creationDate = creationDate;
		this.subject = subject;
		this.content = content;
	}
	
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String creationDateToString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
		return dateFormat.format(creationDate);
	}
}
