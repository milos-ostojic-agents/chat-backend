package models;

import java.io.Serializable;

import javax.ejb.Stateful;
import javax.ejb.EJB;
import beans.JMSAgentBean;

@Stateful
public class Agent implements Serializable{

	/**
	 * 
	 */
	@EJB
	protected JMSAgentBean jms;
	
	private static final long serialVersionUID = 1L;
	protected AID id;
	
	
	
	public Agent() {}
	
	public Agent(AID id) {
		this.id = id;
	}
	
	public AID getId() {
		return id;
	}
	
	public void setId(AID id) {
		this.id = id;
	}
	
	public void handleMessage(ACLMessage message) {}
}
