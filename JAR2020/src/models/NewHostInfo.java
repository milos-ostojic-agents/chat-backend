package models;

import java.io.Serializable;
import java.util.List;

public class NewHostInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<User> loggedInUsers;
	private List<Host> registeredNodes;
	
	public NewHostInfo() {}
	
	public NewHostInfo(List<User> users, List<Host> hosts) {
		this.loggedInUsers = users;
		this.registeredNodes = hosts;
	}
	
	public List<User> getLoggedInUsers() {
		return loggedInUsers;
	}
	public void setLoggedInUsers(List<User> loggedInUsers) {
		this.loggedInUsers = loggedInUsers;
	}
	public List<Host> getRegisteredNodes() {
		return registeredNodes;
	}
	public void setRegisteredNodes(List<Host> registeredNodes) {
		this.registeredNodes = registeredNodes;
	}
	
	
}
