package models;

import javax.websocket.Session;

public class UserSession {
	private String username;
	private Session session;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	
	public UserSession() {}
	
	public UserSession(Session session) {
		this.session = session;
	}
}
