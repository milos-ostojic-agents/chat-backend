package ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import helpers.Parser;
import models.Message;
import models.User;
import models.UserSession;

@Singleton
@ServerEndpoint("/ws")
@LocalBean
public class WSEndPoint {
	static List<UserSession> userSessions = new ArrayList<UserSession>();
	static Parser parser = new Parser();
	
	@OnOpen
	public void onOpen(Session session) {
		boolean sessionExists = userSessions.stream().anyMatch(us -> us.getSession().getId().equals(session.getId()));
		if (!sessionExists) userSessions.add(new UserSession(session));
	}
	
	@OnMessage
	public void verifyUser(Session session, String username) {
		if (!session.isOpen()) return;
		
		for (UserSession us: userSessions) {
			if (us.getSession().getId().equals(session.getId())) {
				us.setUsername(username);
				return;
			}
		}				
	}
	
	public void sendMessage(Message message) {
		try {
			for (UserSession us: userSessions) {
				if (message.getRecipient().equals(us.getUsername())) {
					us.getSession().getBasicRemote().sendText(parser.messageToJSON(message));
					return;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendUserList(List<User> users) {
		try {
			for (UserSession us: userSessions) {
				us.getSession().getBasicRemote().sendText(parser.userListToJSON(users));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@OnClose
	public void close(Session session) {
		removeSession(session);
	}
	
	@OnError
	public void error(Session session, Throwable t) {
		removeSession(session);
		t.printStackTrace();
	}
	
	private void removeSession(Session session) {
		for (UserSession userSession : userSessions) {
			if (userSession.getSession().getId().equals(session.getId())) {
				userSessions.remove(userSession);
				break;
			}
		}
	}
}
