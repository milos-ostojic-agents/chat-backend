package ws;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import models.Message;
import models.User;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/mojQueue")
})
public class QueueMDB implements MessageListener {
	@EJB WSEndPoint ws;
	
	@Override
	public void onMessage(javax.jms.Message msg) {
		ObjectMessage objMessage = (ObjectMessage)msg;
		try {
			Serializable obj = objMessage.getObject();
			if (obj.getClass().equals(Message.class)) ws.sendMessage((Message)obj);
			else ws.sendUserList((ArrayList<User>)obj);
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}

}