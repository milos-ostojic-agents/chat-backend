package ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.websocket.server.ServerEndpoint;

import helpers.Parser;
import models.ACLMessage;
import models.Agent;
import models.AgentType;
import models.Message;
import models.User;
import models.UserSession;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@Singleton
@ServerEndpoint("/agentsws")
@LocalBean
public class WSAgentEndPoint {
	static List<Session> sessions = new ArrayList<Session>();
	static Parser parser = new Parser();
	
	@OnOpen
	public void onOpen(Session session) {
		sessions.add(session);
	}
	
	@OnMessage
	public void doNothing(Session session, String message) {}
	
	public void sendRunningAgents(List<Agent> runningAgents) {
		try {
			for (Session session: sessions) {
				session.getBasicRemote().sendText(parser.agentListToJSON(runningAgents));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void sendMessage(ACLMessage message) {		
		try {
			for (Session session: sessions) {
				session.getBasicRemote().sendText(parser.aclToJSON(message));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@OnClose
	public void close(Session session) {
		removeSession(session);
	}
	
	@OnError
	public void error(Session session, Throwable t) {
		removeSession(session);
		t.printStackTrace();
	}
	
	private void removeSession(Session session) {
		for (Session s : sessions) {
			if (s.getId().equals(session.getId())) {
				sessions.remove(s);
				break;
			}
		}
	}
}
