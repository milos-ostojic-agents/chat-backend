package ws;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import models.ACLMessage;
import models.User;
import models.Agent;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/agentQueue")
})
public class QueueAgentMDB implements MessageListener {
	@EJB WSAgentEndPoint ws;
	
	@Override
	public void onMessage(Message msg) {
		ObjectMessage objMessage = (ObjectMessage)msg;
		try {
			Serializable obj = objMessage.getObject();
			if (obj.getClass().equals(ACLMessage.class)) ws.sendMessage((ACLMessage)obj);
			else ws.sendRunningAgents((ArrayList<Agent>)obj);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
