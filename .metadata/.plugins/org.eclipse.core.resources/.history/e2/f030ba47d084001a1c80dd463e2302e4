package beans;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import models.Host;
import models.Message;
import models.User;
import ws.WSEndPoint;

@Stateless
@Path("/chat")
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ChatBean {
	@EJB
	DataServiceBean dataService;
	@EJB
	WSEndPoint wsEndPoint;
	@EJB
	RestClientBean client;
	
	@POST
	@Path("/users/register")
	public boolean register(User user) {
		return dataService.registerUser(user.getUsername(), user.getPassword());
	}
	
	@POST
	@Path("/users/login")
	public boolean logIn(User user) {
		boolean loginSuccessfull = dataService.logInUser(user.getUsername(), user.getPassword());
		
		if (loginSuccessfull) {
			sendLoggedInUsersToHosts();
			
			wsEndPoint.sendUserList(dataService.getLoggedInUsers());
		}
		
		return loginSuccessfull;
	}
	
	@GET
	@Path("/users/loggedIn")
	public List<User> getLoggedInUsers() {
		return dataService.getLoggedInUsers();
	}
	
	@GET
	@Path("/users/registered")
	public List<User> getRegisteredUsers() {
		return dataService.getRegisteredUsers();
	}
	
	@POST
	@Path("/messages/all/onlyLoggedIn/{onlyLoggedIn}")
	public void sendMessageToAll(Message message, @PathParam("onlyLoggedIn") boolean onlyLoggedIn) {
		List<Message> messages = dataService.sendMessageToAllUsers(message, onlyLoggedIn);
		
		for (Message msg : messages) {
			wsEndPoint.sendMessage(msg);
		}
	}
	
	@POST
	@Path("/messages/user")
	public void sendMessage(Message message) {
		dataService.sendMessage(message);
		
		
		
		wsEndPoint.sendMessage(message);
	}
	
	@GET
	@Path("/messages/{user}")
	public List<Message> getUserMessages(@PathParam("user") String username) {
		return dataService.getAllMessagesForUser(username);
	}
	
	@DELETE
	@Path("/users/loggedIn/{user}")
	public void logOutUser(@PathParam("user") String username) {
		dataService.logOutUser(username);
		
		sendLoggedInUsersToHosts();
		
		wsEndPoint.sendUserList(dataService.getLoggedInUsers());
	}
	
	private void sendLoggedInUsersToHosts() {
		List<Host> hosts = dataService.getRegisteredNodes();
		List<User> loggedInUsers = dataService.getLoggedInUsers();
		for (Host host : hosts) {
			client.sendLoggedInUsers(loggedInUsers, host);
		}
	}
}