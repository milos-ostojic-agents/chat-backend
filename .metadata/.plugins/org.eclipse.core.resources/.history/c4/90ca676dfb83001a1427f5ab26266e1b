package ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import models.UserSession;

@Singleton
@ServerEndpoint("/ws")
@LocalBean
public class WSEndPoint {
	static List<UserSession> userSessions = new ArrayList<UserSession>();
	
	@OnOpen
	public void onOpen(Session session) {
		boolean sessionExists = userSessions.stream().anyMatch(us -> us.getSession().equals(session));
		if (!sessionExists) userSessions.add(new UserSession(session));
	}
	
	@OnMessage
	public void echoTextMessage(Session session, String msg, boolean last) {
		try {
			if (session.isOpen()) {
				for (Session s: sessions) {
					if (!s.getId().equals(session.getId())) {
						s.getBasicRemote().sendText(msg, last);
					}
				}
			}
		} catch (IOException e) {
			try {
				session.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
	}	
	
	@OnClose
	public void close(Session session) {
		for (UserSession userSession : userSessions) {
			if (userSession.getSession().equals(session)) {
				userSessions.remove(session);
				break;
			}
		}
	}
	
	@OnError
	public void error(Session session, Throwable t) {
		for (UserSession userSession : userSessions) {
			if (userSession.getSession().equals(session)) {
				userSessions.remove(session);
				break;
			}
		}
		t.printStackTrace();
	}
}
